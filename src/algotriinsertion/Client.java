/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algotriinsertion;

import java.util.Objects;

/**
 *
 * @author Chris
 */
public class Client {

    private String nom = "";
    private String prenom = "";
    private int codePostal = 0;

    public Client() {
    }

    public Client(String n, String p, int cp) {
        nom = n;
        prenom = p;
        codePostal = cp;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public int getCodePostal() {
        return codePostal;
    }

    @Override
    public String toString() {
        return String.format("%15s %15s %4d", nom, prenom, codePostal);
    }

    public boolean compare(Client element) {
        return ((this.getCodePostal() > element.getCodePostal())
                ||((this.getCodePostal() == element.getCodePostal()) 
                && (this.getNom().compareTo(element.getNom()) > 0)) );
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.nom);
        hash = 29 * hash + Objects.hashCode(this.prenom);
        hash = 29 * hash + this.codePostal;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.prenom, other.prenom)) {
            return false;
        }
        return this.codePostal == other.codePostal;
    }
    
        
       

}
