package algotriinsertion;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chris
 */
public class TestsTrisInsertionEntiers {
    /**
     * Procédure de test du tri d'un tableau d'entiers
     * @param t le tableau à trier
     * @param t_ok le tableau trié ( =résultat attendu si le tri a bien fonctionné)
     * @param message le message à afficher ( =titre du test en cours)
     */    
    private static void testTriEntiers(int [] t, int [] t_ok, String message){
    	assert t.length==t_ok.length:"le tableau non trié et le tableau attendu n'ont pas la meme taille";
        TriInsertion.triInsertionEntiers(t); //appel de la procédure de tri que l'on souhaite tester
	System.out.printf("\t" + message);
	for (int i=0; i<t.length; i++){
		assert(t[i] == t_ok[i]):"Les elements a l'indice " + i + "sont differents";  
                //comparaison du résultat du tri avec le résultat attendu
	}
	System.out.printf("ok\n");
    }

    /**
     *
     */
    public static void tests(){
        int t_trie[] = {-2, -1, 0, 1, 2};
        int t_trie_ecart[] = {-7, -1, 0, 1, 8};
        int t_inverse[] = {2, 1, 0, -1, -2};
        int t_inverse_ecart[] = {4, 1, 0, -1, -9};
        int t_singleton[] = {0};
        int t_non_trie[] = {7, -5, 0, 4, -12, 18};
        int t_min_max[] = {5, Integer.MIN_VALUE, -78, Integer.MAX_VALUE, 0, 78, 4};
        int t_repetition_pure[] = {6, 6, 6, 6};
        int t_nombre_repete[] = {7, -5, 0, -5, 4, -5};

        int t_trie_ok[] = {-2, -1, 0, 1, 2};
        int t_trie_ecart_ok[] = {-7, -1, 0, 1, 8};
        int t_inverse_ok[] = {-2, -1, 0, 1, 2};
        int t_inverse_ecart_ok[] = {-9, -1, 0, 1, 4};
        int t_singleton_ok[] = {0};
        int t_non_trie_ok[] = {-12, -5, 0, 4, 7, 18};
        int t_min_max_ok[] = {Integer.MIN_VALUE, -78, 0, 4, 5, 78, Integer.MAX_VALUE};
        int t_repetition_pure_ok[] = {6, 6, 6, 6};
        int t_nombre_repete_ok[] = {-5, -5, -5, 0, 4, 7};

        System.out.printf("\n\nTests unitaires pour TriInsertionEntiers\n");
        System.out.printf("-----------------------------------\n");
        System.out.printf("1. Testons avec differents tableaux\n\n");

        testTriEntiers(t_trie, t_trie_ok, "\tTableau trie dont les numeros se suivent... ");
        testTriEntiers(t_trie_ecart, t_trie_ecart_ok, "\tTableau trie dont les numeros ne se suivent pas... ");
        testTriEntiers(t_inverse, t_inverse_ok, "\tTableau trie en ordre inverse (numeros se suivent)... ");
        testTriEntiers(t_inverse_ecart, t_inverse_ecart_ok, "\tTableau trie en ordre inverse (numeros ne se suivent pas)... ");
        testTriEntiers(t_singleton, t_singleton_ok, "\tTableau singleton... ");
        testTriEntiers(t_non_trie, t_non_trie_ok, "\tTableau non trie ... ");
        testTriEntiers(t_min_max, t_min_max_ok, "\tTableau contenant les valeurs entieres extremes... ");
        testTriEntiers(t_repetition_pure, t_repetition_pure_ok, "\tTableau avec un nombre unique repete... ");
        testTriEntiers(t_nombre_repete, t_nombre_repete_ok, "\tTableau non trié avec un nombre repete... ");
        
        }
}
