/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algotriinsertion;

/**
 *
 * @author Chris
 */
public class TriInsertion {

    public static void triInsertionClients(Client[] fiches) {
        for (int i = 1; i < fiches.length; ++i) {
            inserer(fiches[i], fiches, i);
        }
    }

    public static void triInsertionEntiers(int[] fiches) {
        for (int i = 1; i < fiches.length; ++i) {
            inserer(fiches[i], fiches, i);
        }
    }

    private static void inserer(int element, int tab[], int tailleGauche) {
        int j;
        for (j = tailleGauche; j > 0 && tab[j - 1] > element; j--) {
            tab[j] = tab[j - 1];
        }
        tab[j] = element;
    }

    private static void inserer(Client element, Client tab[], int tailleGauche) {
        int j;
        for (j = tailleGauche; j > 0 && tab[j-1].compare(element); j--) {
            tab[j] = tab[j - 1];
        }
        tab[j] = element;
        
    }
}
