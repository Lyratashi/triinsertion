package algotriinsertion;

/*

*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chris
 */
public class TestsTrisInsertionClients implements ListeClient {
    /**
     * Procédure de test du tri d'un tableau de clients
     * @param t le tableau à trier
     * @param t_ok le tableau trié ( =résultat attendu si le tri a bien fonctionné)
     * @param message le message à afficher ( =titre du test en cours)
     */    
    private static void testTriClients(Client [] t, Client [] t_ok, String message){
    	    	assert t.length==t_ok.length:"le tableau non trié et le tableau attendu n'ont pas la meme taille\n";
        TriInsertion.triInsertionClients(t); //appel de la procédure de tri que l'on souhaite tester
	System.out.printf("\t" + message);
	for (int i=0; i<t.length; i++){
		assert(t[i].equals(t_ok[i])):"Les elements a l'indice " + t[i] + " "+t_ok[i] + "sont differents";  
                //comparaison du résultat du tri avec le résultat attendu
	}
	System.out.printf("ok\n");
    }

    /**
     *
     */
    public static void tests(){
        Client t_trie[] = {CLIENT_1, CLIENT_2, CLIENT_3, CLIENT_4, CLIENT_5};
        Client  t_trie_ecart[] = {CLIENT_3, CLIENT_9, CLIENT_10, CLIENT_11, CLIENT_15};
        Client t_inverse[] = {CLIENT_6, CLIENT_5, CLIENT_4, CLIENT_3, CLIENT_2};
        Client t_inverse_ecart[] = {CLIENT_11, CLIENT_5, CLIENT_4, CLIENT_3, CLIENT_1};
        Client t_singleton[] = {CLIENT_1};
        Client t_non_trie[] = {CLIENT_7, CLIENT_5, CLIENT_1, CLIENT_4, CLIENT_12, CLIENT_15};
        Client t_min_max[] = {CLIENT_5, CLIENT_1, CLIENT_7, CLIENT_15, CLIENT_2, CLIENT_8, CLIENT_14};
        Client t_repetition_pure[] = {CLIENT_6, CLIENT_6, CLIENT_6, CLIENT_6};
        Client t_nombre_repete[] = {CLIENT_7, CLIENT_5, CLIENT_8, CLIENT_5, CLIENT_9, CLIENT_5};

        Client t_trie_ok[] = {CLIENT_1, CLIENT_2, CLIENT_3, CLIENT_4, CLIENT_5};
        Client t_trie_ecart_ok[] = {CLIENT_3, CLIENT_9, CLIENT_10, CLIENT_11, CLIENT_15};
        Client t_inverse_ok[] = {CLIENT_2, CLIENT_3, CLIENT_4, CLIENT_5, CLIENT_6};
        Client t_inverse_ecart_ok[] = {CLIENT_1, CLIENT_3, CLIENT_4, CLIENT_5, CLIENT_11};
        Client t_singleton_ok[] = {CLIENT_1};
        Client t_non_trie_ok[] = {CLIENT_1, CLIENT_4, CLIENT_5, CLIENT_7, CLIENT_12, CLIENT_15};
        Client t_min_max_ok[] = {CLIENT_1, CLIENT_2, CLIENT_5, CLIENT_7, CLIENT_8, CLIENT_14, CLIENT_15};
        Client t_repetition_pure_ok[] = {CLIENT_6, CLIENT_6, CLIENT_6, CLIENT_6};
        Client t_nombre_repete_ok[] = {CLIENT_5, CLIENT_5, CLIENT_5, CLIENT_7, CLIENT_8, CLIENT_9};
        
                System.out.printf("\n\nTests unitaires pour TriInsertionClients\n");
        System.out.printf("-----------------------------------\n");
        System.out.printf("1. Testons avec differents tableaux\n\n");

        testTriClients(t_trie, t_trie_ok, "\tTableau trie dont les numeros se suivent... ");
        testTriClients(t_trie_ecart, t_trie_ecart_ok, "\tTableau trie dont les numeros ne se suivent pas... ");
        testTriClients(t_inverse, t_inverse_ok, "\tTableau trie en ordre inverse (numeros se suivent)... ");
        testTriClients(t_inverse_ecart, t_inverse_ecart_ok, "\tTableau trie en ordre inverse (numeros ne se suivent pas)... ");
        testTriClients(t_singleton, t_singleton_ok, "\tTableau singleton... ");
        testTriClients(t_non_trie, t_non_trie_ok, "\tTableau non trie ... ");
        testTriClients(t_min_max, t_min_max_ok, "\tTableau contenant les valeurs entieres extremes... ");
        testTriClients(t_repetition_pure, t_repetition_pure_ok, "\tTableau avec un nombre unique repete... ");
        testTriClients(t_nombre_repete, t_nombre_repete_ok, "\tTableau non trié avec un nombre repete... ");
        
        }
}
